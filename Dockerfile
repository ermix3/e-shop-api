FROM eclipse-temurin:21-jre-alpine

VOLUME /tmp

COPY target/*.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]

# Old Version
#FROM maven:3-jdk-8-alpine AS builder
#
#WORKDIR /usr/src/app
#
#COPY . /usr/src/app
#RUN mvn package

#FROM openjdk:8-jre-alpine
#
#COPY --from=builder /usr/src/app/target/*.jar /app.jar
#
#EXPOSE 8080
#
#ENTRYPOINT ["java"]
#CMD ["-jar", "/app.jar"]
