package com.ermix.e_shop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {
    private Long id;
    private String name;
    private String description;
    private double price;
    private int actualStock;
    private int alertStock;
    private LocalDate dateExpiration;
    private String image;
    private boolean isVisible;
    private String comments;
}
