package com.ermix.e_shop.mapper;


import com.ermix.e_shop.dao.User;
import com.ermix.e_shop.dto.UserDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
     UserDTO toUserDTO(User user);
}
