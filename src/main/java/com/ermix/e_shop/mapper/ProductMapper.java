package com.ermix.e_shop.mapper;

import com.ermix.e_shop.dao.Product;
import com.ermix.e_shop.dto.ProductDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductDTO toProductDTO(Product product);
}
