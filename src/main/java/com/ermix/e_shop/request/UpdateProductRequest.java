package com.ermix.e_shop.request;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UpdateProductRequest {
    private Long id;
    private String name;
    private String description;
    private double price;
    private int actualStock;
    private int alertStock;
    private LocalDate dateExpiration;
    private String image;
    private boolean isVisible;
    private String comments;
}
