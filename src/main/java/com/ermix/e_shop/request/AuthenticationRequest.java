package com.ermix.e_shop.request;

import lombok.Data;

@Data
public class AuthenticationRequest {
    private String email;
	private String password;
}
