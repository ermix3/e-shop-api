package com.ermix.e_shop.controller;

import com.ermix.e_shop.dto.ProductDTO;
import com.ermix.e_shop.request.ProductRequest;
import com.ermix.e_shop.service.ProductServiceImp;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/products")
public class ProductController {
    private final ProductServiceImp productService;

    @PostMapping()
    public ResponseEntity<ProductDTO> addProduct(@RequestBody ProductRequest productRequest, HttpServletRequest request) {
//        Long idUser = Long.parseLong(request.getHeaders("x-auth-user-id").nextElement());
        return ResponseEntity.ok(productService.addProduct(productRequest));
    }

    @GetMapping()
    public ResponseEntity<List<ProductDTO>> getProducts(HttpServletRequest request) {
        return ResponseEntity.ok(productService.getAllProducts());
    }

    @GetMapping("/{productId}")
    public ResponseEntity<ProductDTO> getProduct(@PathVariable Long productId, HttpServletRequest request) {
        try {
            return ResponseEntity.ok(productService.getProduct(productId));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<String> deleteProduct(@PathVariable Long productId, HttpServletRequest request) {
        try {
            productService.deleteProduct(productId);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{productId}")
    public ResponseEntity<ProductDTO> updateProduct(@PathVariable Long productId, @RequestBody ProductRequest productRequest, HttpServletRequest request) {
        try {
            return ResponseEntity.ok(productService.updateProduct(productId, productRequest));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

}
