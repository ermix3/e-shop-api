package com.ermix.e_shop.controller;

import com.ermix.e_shop.dao.User;
import com.ermix.e_shop.dto.LoginDTO;
import com.ermix.e_shop.dto.UserDTO;
import com.ermix.e_shop.request.AuthenticationRequest;
import com.ermix.e_shop.request.SignupRequest;
import com.ermix.e_shop.service.AuthenticationService;
import com.ermix.e_shop.service.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthenticationController {
    private final UserDetailsService userDetailsService;
    private final AuthenticationService userService;
    private final JwtService jwtService;


    @PostMapping("register")
    public ResponseEntity<UserDTO> register(@RequestBody SignupRequest signupRequest) {
        return ResponseEntity.ok(userService.signup(signupRequest));
    }

    @PostMapping("log-in")
    public ResponseEntity<LoginDTO> login(@RequestBody AuthenticationRequest authenticationRequest) {
        User user = userService.authenticate(authenticationRequest);
        String jwtToken = jwtService.generateToken(user);

        LoginDTO response = LoginDTO.builder()
                .token(jwtToken)
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("userinfo")
    public ResponseEntity<?> getUserInfo(Principal user) {
        User userObj = (User) userDetailsService.loadUserByUsername(user.getName());

        UserDTO userInfo = UserDTO.builder()
                .username(userObj.getUsername())
                .firstName("John")
                .lastName("Doe")
//                .roles(userObj.getAuthorities())
                .build();

        return ResponseEntity.ok(userInfo);
    }

    @GetMapping("log-out")
    public ResponseEntity<?> logout() {
        return ResponseEntity.ok().build();
    }

}
