package com.ermix.e_shop.service;

import com.ermix.e_shop.dto.ProductDTO;
import com.ermix.e_shop.request.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    ProductDTO addProduct(ProductRequest productRequest);

    void deleteProduct(Long productId);

    ProductDTO updateProduct(Long productId, ProductRequest productRequest);

    ProductDTO getProduct(Long productId);

    List<ProductDTO> getAllProducts();

//    List<ProductDTO> getProductsByCategory();
}
