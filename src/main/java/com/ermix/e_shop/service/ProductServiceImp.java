package com.ermix.e_shop.service;

import com.ermix.e_shop.dao.Product;
import com.ermix.e_shop.dto.ProductDTO;
import com.ermix.e_shop.mapper.ProductMapper;
import com.ermix.e_shop.repository.ProductRepository;
import com.ermix.e_shop.request.ProductRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImp implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Override
    public ProductDTO addProduct(ProductRequest productRequest) {
        Product product = Product.builder()
                .name(productRequest.getName())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .actualStock(productRequest.getActualStock())
                .alertStock(productRequest.getAlertStock())
                .dateExpiration(productRequest.getDateExpiration())
                .image(productRequest.getImage())
                .isVisible(productRequest.isVisible())
                .comments(productRequest.getComments())
                .build();
        productRepository.save(product);
        return productMapper.toProductDTO(product);
    }

    @Override
    public void deleteProduct(Long productId) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product not found"));
        productRepository.delete(product);
    }

    @Override
    public ProductDTO updateProduct(Long productId, ProductRequest productRequest) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product not found"));

        product.setName(productRequest.getName());
        product.setDescription(productRequest.getDescription());
        product.setPrice(productRequest.getPrice());
        product.setActualStock(productRequest.getActualStock());
        product.setAlertStock(productRequest.getAlertStock());
        product.setDateExpiration(productRequest.getDateExpiration());
        product.setImage(productRequest.getImage());
        product.setVisible(productRequest.isVisible());
        product.setComments(productRequest.getComments());
        productRepository.save(product);

        return productMapper.toProductDTO(product);
    }

    @Override
    public ProductDTO getProduct(Long productId) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product not found"));
        return productMapper.toProductDTO(product);
    }

    @Override
    public List<ProductDTO> getAllProducts() {
        return productRepository
                .findAll()
                .stream()
                .map(productMapper::toProductDTO)
                .toList();
    }
}
