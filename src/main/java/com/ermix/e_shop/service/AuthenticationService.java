package com.ermix.e_shop.service;

import com.ermix.e_shop.dao.User;
import com.ermix.e_shop.dto.UserDTO;
import com.ermix.e_shop.mapper.UserMapper;
import com.ermix.e_shop.repository.UserDetailsRepository;
import com.ermix.e_shop.request.AuthenticationRequest;
import com.ermix.e_shop.request.SignupRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserDetailsRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final UserMapper userMapper;


    public UserDTO signup(SignupRequest input) {
        var user = User.builder()
                .username(input.getEmail())
                .password(passwordEncoder.encode(input.getPassword()))
                .build();

        return userMapper.toUserDTO(userRepository.save(user));
    }

    public User authenticate(AuthenticationRequest input) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        input.getEmail(),
                        input.getPassword()
                )
        );

        return userRepository.findByEmail(input.getEmail()).orElseThrow();
    }
}