package com.ermix.e_shop.service;

import com.ermix.e_shop.dao.User;
import com.ermix.e_shop.dto.UserDTO;
import com.ermix.e_shop.mapper.UserMapper;
import com.ermix.e_shop.repository.UserDetailsRepository;
import com.ermix.e_shop.request.AuthenticationRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImp implements UserDetailsService {
    private final UserDetailsRepository userDetailsRepository;
    private final UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDetailsRepository.findByUsername(username);

        if (null == user) {
            throw new UsernameNotFoundException("User Not Found with userName " + username);
        }
        return user;
    }


    public UserDTO register(AuthenticationRequest authenticationRequest) {
        User user = User.builder()
                .username(authenticationRequest.getEmail())
                .password(authenticationRequest.getPassword())
                .build();
        userDetailsRepository.save(user);
        return userMapper.toUserDTO(user);
    }
}
