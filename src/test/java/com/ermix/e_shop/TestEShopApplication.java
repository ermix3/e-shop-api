package com.ermix.e_shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class TestEShopApplication {

    public static void main(String[] args) {
        SpringApplication.from(EShopApplication::main)
                .with(TestcontainersConfiguration.class)
                .run(args);
    }
}