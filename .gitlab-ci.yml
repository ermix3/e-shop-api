workflow:
  name: "${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == "dev"'
      variables:
        IMAGE_TAG: "dev-${CI_COMMIT_SHORT_SHA}"
        PROFILE: "dev"
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == "stg"'
      variables:
        IMAGE_TAG: "stg-${CI_COMMIT_SHORT_SHA}"
        PROFILE: "stg"
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == "master"'
      variables:
        IMAGE_TAG: "prod-${CI_COMMIT_SHORT_SHA}"
        PROFILE: "prod"

    - when: never # Prevent pipeline from running on other branches

variables:
  MAVEN_OPTS: >-
    -Dhttps.protocols=TLSv1.2
    -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
    -Dorg.slf4j.simpleLogger.showDateTime=true
    -Djava.awt.headless=true
  MAVEN_CLI_OPTS: >-
    --batch-mode
    --errors
    --fail-at-end
    --show-version
    --no-transfer-progress
    -DinstallAtEnd=true
    -DdeployAtEnd=true
  CACHE_VERSION: "v1" # Update this if cache needs to be reset
  DOCKER_IMAGE: "ermix3/e-shop-api"
  DOCKER_IMAGE_NAME: "${DOCKER_IMAGE}:${DOCKER_TAG}"

image: maven:3.9.6-eclipse-temurin-21-alpine

stages:
  - compile
  - test
  - package
  - publish
  - deploy

cache:
  key: "$CACHE_VERSION-maven-cache"
  paths:
    - .m2/repository

before_script:
  - echo "🚀 Starting CI pipeline for $CI_PROJECT_NAME on branch $CI_COMMIT_REF_NAME"

compile:
  stage: compile
  script:
    - echo "🏗️ Compiling the project..."
    - mvn $MAVEN_CLI_OPTS clean compile -Dspring-boot.run.profiles=$PROFILE
  artifacts:
    paths:
      - target/*.jar
    expire_in: 1 day

test:
  stage: test
  needs: [ compile ]
  services:
    - docker:24.0.5-dind
  variables:
    TESTCONTAINERS_RYUK_DISABLED: "true"
    RYUK_CONTAINER_PRIVILEGED: "true"
    DOCKER_HOST: "tcp://docker:2375"
    DOCKER_TLS_CERTDIR: ""
  script:
    - echo "🧪 Running tests with Testcontainers..."
    - mvn $MAVEN_CLI_OPTS test

package:
  stage: package
  needs: [ test ]
  script:
    - echo "📦 Packaging the application into a JAR file..."
    - mvn $MAVEN_CLI_OPTS clean package -DskipTests -Dspring-boot.run.profiles=$PROFILE
  artifacts:
    paths:
      - target/*.jar
    expire_in: 1 day

docker_build_push:
  stage: publish
  image: docker:24.0.5
  services:
    - docker:24.0.5-dind
  before_script:
    - echo "🔐 Logging into Docker Hub..."
    - echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin
  script:
    - echo "🏗️ Building Docker image..."
    - docker build -t "$DOCKER_IMAGE_NAME" .
    - echo "🚢 Pushing Docker image to Docker Hub..."
    - docker push "$DOCKER_IMAGE_NAME"
    - if [ "$CI_COMMIT_REF_NAME" == "master" ]; then
      docker tag "$DOCKER_IMAGE_NAME" "$DOCKER_IMAGE:latest" && docker push "$DOCKER_IMAGE:latest";
      fi

deploy:
  stage: deploy
  needs: [ docker_build_push ]
  script:
    - echo "🚀 Deploying the project..."
  #    - if [ -f ci_settings.xml ]; then
  #        mvn $MAVEN_CLI_OPTS deploy --settings ci_settings.xml;
  #      else
  #        mvn $MAVEN_CLI_OPTS deploy;
  #      fi
  #  environment:
  #    name: production
  #    url: https://your-production-url.com
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
#  when: manual
#  tags:
#    - docker
